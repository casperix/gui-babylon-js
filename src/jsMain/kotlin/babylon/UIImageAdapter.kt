package babylon

import BABYLON.GUI.Container
import BABYLON.GUI.Image
import casper.core.Disposable
import casper.format.toHexString
import casper.gui.component.UIImage
import casper.types.*

internal class UIImageAdapter(val source: Container, override val component: UIImage) : Disposable, ComponentAdapter<UIImage> {
	private val image = Image("image")

	init {
		//	image.detectPointerOnOpaqueOnly = true
		image.isPointerBlocker = false
		image.isHitTestVisible = false
		image.width = "100%"
		image.height = "100%"
		image.zIndex = -3.0
		source.addControl(image)
	}

	override fun dispose() {
		image.dispose()
	}

	override fun invalidate() {
		val fillStyle = component.fillStyle
		if (fillStyle is Scale9Texture) {
			image.paddingLeftInPixels = fillStyle.border
			image.paddingRightInPixels = fillStyle.border
			image.paddingTopInPixels = fillStyle.border
			image.paddingBottomInPixels = fillStyle.border

			image.stretch = Image.STRETCH_NINE_PATCH
			image.populateNinePatchSlicesFromImage = false
			image.sliceLeft = fillStyle.scale9.left.toDouble()
			image.sliceRight = fillStyle.scale9.right.toDouble()
			image.sliceTop = fillStyle.scale9.top.toDouble()
			image.sliceBottom = fillStyle.scale9.bottom.toDouble()
			image.source = fillStyle.texture.url
			image.alpha = 1.0
		} else if (fillStyle is ScaleTexture) {
			image.paddingLeftInPixels = fillStyle.border
			image.paddingRightInPixels = fillStyle.border
			image.paddingTopInPixels = fillStyle.border
			image.paddingBottomInPixels = fillStyle.border

			image.stretch = Image.STRETCH_FILL
			image.source = fillStyle.texture.url
			image.alpha = 1.0
		} else if (fillStyle is ColorFill) {
			image.stretch = Image.STRETCH_FILL
			image.source = ""
			source.background = "#" + fillStyle.color.toHexString()
//			source.alpha = fillStyle.color.w
		} else {
			throw Error("Unsupported fill style: $fillStyle")
		}
	}
}
