package babylon

import BABYLON.GUI.Container
import BABYLON.GUI.Control
import casper.gui.UINode
import casper.gui.UINodeFactory

class BabylonNodeFactory(val uiScene:BabylonUIScene) : UINodeFactory {
	companion object {
		var lastId = 0
	}
	override fun create(): UINode {
		val element = Container("node-" + (++lastId))

		element.isPointerBlocker = false
		element.isHitTestVisible = false

		element.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP
		element.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT

		element.left = "0px"
		element.top = "0px"
		element.width = "100px"
		element.height = "100px"

		return BabylonUINode( uiScene, element)
	}
}