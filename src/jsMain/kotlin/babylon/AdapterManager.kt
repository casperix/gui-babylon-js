package babylon

import BABYLON.GUI.Container
import casper.collection.ObservableCollection
import casper.gui.component.UIComponent
import casper.gui.component.UIImage
import casper.gui.component.text.UIText
import casper.gui.component.text.UITextInput

internal class AdapterManager(val uiScene: BabylonUIScene, val source: Container, val components: ObservableCollection<UIComponent>) {
	private var textAdapter: ComponentAdapter<UIText>? = null
	private var imageAdapter: ComponentAdapter<UIImage>? = null
	private var textInputAdapter: ComponentAdapter<UITextInput>? = null

	fun dispose() {
		textAdapter?.dispose()
		textAdapter = null
		imageAdapter?.dispose()
		imageAdapter = null
		textInputAdapter?.dispose()
		textInputAdapter = null
	}

	fun update() {
		val text: UIText? = components.filterIsInstance<UIText>().firstOrNull()
		textAdapter = syncAdapter(textAdapter, text, { component -> UITextAdapter(uiScene,source, component) })

		val image: UIImage? = components.filterIsInstance<UIImage>().firstOrNull()
		imageAdapter = syncAdapter(imageAdapter, image, { component -> UIImageAdapter(source, component) })

		val textInput: UITextInput? = components.filterIsInstance<UITextInput>().firstOrNull()
		textInputAdapter = syncAdapter(textInputAdapter, textInput, { component -> UITextInputAdapter(source, component) })
	}

	private fun <Component : UIComponent> syncAdapter(lastAdapter: ComponentAdapter<Component>?, component: Component?, builder: (Component) -> ComponentAdapter<Component>): ComponentAdapter<Component>? {
		if (component != null) {
			if (lastAdapter == null || lastAdapter.component != component) {
				lastAdapter?.dispose()
				val nextAdapter = builder(component)
				nextAdapter.invalidate()
				return nextAdapter
			}
			lastAdapter.invalidate()
			return lastAdapter
		} else {
			lastAdapter?.dispose()
			return null
		}
	}
}