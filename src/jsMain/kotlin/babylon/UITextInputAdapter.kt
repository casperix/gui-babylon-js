package babylon

import BABYLON.GUI.Container
import BABYLON.GUI.Control
import BABYLON.GUI.InputText
import casper.core.Disposable
import casper.format.toHexString
import casper.gui.component.text.UITextInput
import casper.types.unsetAlpha

internal class UITextInputAdapter(val source: Container, override val component: UITextInput) : Disposable, ComponentAdapter<UITextInput> {
	val textInput = InputText("text")

	init {
		textInput.isPointerBlocker = true
		textInput.isHitTestVisible = true

		textInput.focusedBackground = ""
		textInput.background = ""
		textInput.thickness = 0.0

		textInput.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP
		textInput.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT
		textInput.width = "1.0"
		textInput.height = "1.0"
		textInput.text = component.onText.value
		textInput.onFocusObservable.add({ self, _ -> component.onFocused.set(true) })
		textInput.onBlurObservable.add({ self, _ -> component.onFocused.set(false) })
		textInput.onTextChangedObservable.add({ self, _ -> component.onText.set(self.text) })
		textInput.zIndex = -2.0
		source.addControl(textInput)
	}

	override fun dispose() {
		textInput.dispose()
	}

	override fun invalidate() {
		val fontProperty = component.fontProperty
		textInput.fontSize = fontProperty.size
		textInput.fontFamily = fontProperty.name

		val colorProperty = component.colorProperty
		textInput.color = "#" + colorProperty.color.unsetAlpha().toHexString()

		val inputProperty = component.inputProperty
		textInput.highligherOpacity = 1.0 - inputProperty.selectionColor.w
		textInput.textHighlightColor = "#" + inputProperty.selectionColor.unsetAlpha().toHexString()
	}
}