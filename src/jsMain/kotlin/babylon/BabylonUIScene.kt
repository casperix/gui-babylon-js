package babylon

import BABYLON.GUI.AdvancedDynamicTexture
import BABYLON.Scene
import casper.gui.UIScene
import casper.scene.BabylonSceneDispatcher
import casper.signal.concrete.Signal
import casper.signal.concrete.StorageSignal
import kotlin.math.roundToInt
import kotlin.math.roundToLong

class BabylonUIScene(val scene: Scene) : UIScene {
	val uiScreenTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI");
	override val root = BabylonUINode(this, uiScreenTexture.rootContainer)
	override val sceneDispatcher = BabylonSceneDispatcher(scene)

	override val dispatcher = BabylonUIDispatcher(scene)
	override val nodeFactory = BabylonNodeFactory(this)

	override val onFrame = Signal<Int>()

	init {
		uiScreenTexture.rootContainer.name = "ROOT"
		uiScreenTexture.useInvalidateRectOptimization = false
		scene.onBeforeRenderObservable.add({ _, _ ->
			val w = scene.getEngine().getRenderWidth().roundToInt()
			val h = scene.getEngine().getRenderHeight().roundToInt()
			root.setSize(w, h)

			val deltaTime = scene.getEngine().getDeltaTime().roundToInt()
			onFrame.set(deltaTime)
		})
	}
}